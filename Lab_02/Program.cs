﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02
{
    
    class Program
    {
        private static void EnterData(out double xMin, out double x2Min, out double xMax, out double x2Max, out double dx, out double dx2)
        {
            Console.Write("Enter xMin:");
            xMin = double.Parse(Console.ReadLine().Replace('.', ','));
            Console.Write("Enter x2Min:");
            x2Min = double.Parse(Console.ReadLine().Replace('.', ','));

            Console.Write("Enter xMax:");
            xMax = double.Parse(Console.ReadLine().Replace('.', ','));
            Console.Write("Enter x2Max:");
            x2Max = double.Parse(Console.ReadLine().Replace('.', ','));

            Console.Write("Enter dx:");
            dx = double.Parse(Console.ReadLine().Replace('.', ','));
            Console.Write("Enter dx2:");
            dx2 = double.Parse(Console.ReadLine().Replace('.', ','));
        }

        private static void Cycle( double xMin,  double x2Min,  double xMax,  double x2Max,  double dx, double dx2)
        {
            double x1 = xMin;

            while(x1<=xMax)
            {
                double x2 = x2Min;
                    while(x2 <= x2Max)
                {
                    double a, b, c,d,y;
                    a = x1 + 2 * x2 + 9;
                    b = a / 0.666;
                    c = Math.Exp(b);
                    d = Math.Cos(c);
                    y = Math.Pow(d, 3);
                    Console.WriteLine("X1 =" + x1);
                    Console.WriteLine("X2 =" + x2);
                    Console.WriteLine("Y =" + y);
                    x2 += dx2;
                    for (int i = 0;i < 2; i++) 
                        {
                        double q, w, e, r, Sum;
                        if (a > 0)
                        {
                            q = a;
                        }
                            else
                                {
                                    q = 1;
                                }
                        if (b > 0)
                        {
                            w = b;
                        }
                            else
                                {
                                    w = 1;
                                }
                        if (c > 0)
                        {
                            e = c;
                        }
                            else
                                {
                                    e = 1;
                                }
                        if (d > 0)
                        {
                            r = a;
                        }
                            else
                                {
                                    r = 1;
                                }
                        Sum = q * w * e * r;
                        Console.WriteLine("The sum of all negative values   is" + Sum);
                    } 
                }
                x1 += dx;
            }
            
            
        }
        static void Main(string[] args)
        {
            EnterData(out double xMin, out double x2Min, out double xMax, out double x2Max, out double dx, out double dx2);
            Cycle(xMin, x2Min, xMax, x2Max, dx, dx2);
            Console.ReadKey();
        }
    }
}
